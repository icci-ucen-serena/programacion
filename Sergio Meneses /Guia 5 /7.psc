Algoritmo Ceros_10x10_diagonal_1
	Dimension m[10,10]
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			m[i,j]=0
		FinPara
	FinPara
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			Si i==j Entonces
				m[i,j]=1
			FinSi
		FinPara
	FinPara
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			Escribir Sin Saltar " ",m[i,j]
		FinPara
		Escribir ""
	FinPara
FinAlgoritmo
