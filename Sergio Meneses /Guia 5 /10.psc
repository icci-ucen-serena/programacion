Algoritmo Uno_y_uno
	Dimension v[10,10]
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			Si i%2=0 Entonces
				Si j%2=0 Entonces
					v[i,j]=0
				SiNo
					v[i,j]=1
				FinSi
			SiNo
				Si j%2=0 Entonces
					v[i,j]=1
				SiNo
					v[i,j]=0
				FinSi
			FinSi
			Escribir Sin Saltar " ",v[i,j]
		FinPara
		Escribir ""
	FinPara
FinAlgoritmo

