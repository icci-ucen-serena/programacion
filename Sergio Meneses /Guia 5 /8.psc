Algoritmo Mitad_ceros_mitad_unos
	Dimension b[10,10]
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			b[i,j]=0
		FinPara
	FinPara
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			Si i<j Entonces
				b[i,j]=1
			FinSi
		FinPara
	FinPara
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			Escribir Sin Saltar " ",b[i,j]
		FinPara
		Escribir ""
	FinPara
FinAlgoritmo
