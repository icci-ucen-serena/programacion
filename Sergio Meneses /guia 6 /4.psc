Algoritmo cuatro
	Dimension jl[5,7]
	Dimension t[5,7]
	cont=1
	tb=30
	ta=-150
	db=0
	da=0
	Escribir "TEMPERATURAS JULIO MARTE"
	Para i=0 hasta 4 hacer
		Para j=0 hasta 6 Hacer
			jl[i,j]=cont
			t[i,j]=azar(160)-140
			cont=cont+1
			Si t[i,j]<tb Entonces
				tb=t[i,j]
				db=jl[i,j]
			SiNo
				Si t[i,j]>ta Entonces
					ta=t[i,j]
					da=jl[i,j]
				FinSi
			FinSi
			Si jl[i,j]>31 Entonces
				jl[i,j]=0
				t[i,j]=0
			FinSi
			Escribir Sin Saltar " " jl[i,j] " "
			Escribir Sin Saltar t[i,j] " " " "
		FinPara
		Escribir ""
	FinPara
	
	Escribir ""
	Escribir "El da ", da " es el da con mayor temperatura, alcanzando los ", ta "C"
	Escribir ""
	Escribir "El da ", db " es el da con menor temperatura, alcanzando los ", tb "C"
FinAlgoritmo
