Algoritmo discriminante_P15
	Escribir "Escriba los datos (a, b y c)"
	Leer a, b, c
	aux=b*b-4*a*c
	Si aux<0 Entonces
		Escribir "Sus soluciones son imaginarias y distintas"
	SiNo
		Si aux=0 Entonces
			Escribir "Sus soluciones son reales e iguales"
		SiNo
			Si aux>0 Entonces
				Escribir "Sus soluciones son reales y distintas"
			FinSi
		FinSi
	FinSi
	FinAlgoritmo
