Algoritmo ejercicio_1
	Escribir "�de que tipo es la magnitud a medir?"
	Escribir ""
	Escribir "1) Distancia"
	Escribir "2) peso"
	Escribir "3) velocidad"
	Leer f
	Segun f Hacer
		1:
			Escribir "ingrese la distancia"
			Leer d
			Escribir "�En que unidad esta ese valor?"
			Escribir ""
			Escribir "1. metro (m)"
			Escribir "2. Kilometro (Km)"
			Escribir "3. milla terrestre"
			Escribir "4. milla nautica"
			Leer unid
			Segun unid Hacer
				1:
					km = d/1000
					millt = d/1609.344
					milln = d/1852
					Escribir d " metros equivalen a ", km " Kilometros."
					Escribir "equivalente a ", millt " millas."
					Escribir "y equivalente a ", milln " millas nauticas."
				2:
					m = d*1000
					millt1 = d/1.60934
					milln1 = d/1.852
					Escribir d " Kilometros equivalen a ", m " metros."
					Escribir "equivalente a ", millt1 " millas."
					Escribir "y equivalente a ", milln1 "millas nauticas."
				3:
					m2 = d*1609.344
					km2 = d*1.60934
					milln2 = d/1.15078
					Escribir d " millas equivalen a ", m2 " metros."
					Escribir "equivalente a ", km2 " kilometros."
					Escribir "y equivalente a ", milln2 " millas nauticas"
				4:
					m3 = d*1852
					km3 = d*1.852
					millt3 = d*1.15078
					Escribir d " millas nauticas equivalen a ", m3 " metros."
					Escribir "equivalente a ", km3 " kilometros."
					Escribir "y equivalente a " millt3 " millas"
				De Otro Modo:
					Escribir "opcion no valida"
			Fin Segun
		2:
			Escribir "ingrese el peso"
			Leer p
			Escribir "�En que unidad esta ese valor?"
			Escribir ""
			Escribir "1. gramo (g)"
			Escribir "2. Kilogramo (Kg)"
			Escribir "3. libra"
			Escribir "4. onza"
			Leer unip
			Segun unip Hacer
				1:
					kg = p/1000
					lib = p/453.592
					onz = p/28.3495
					Escribir p " gramos equivalen a: ", kg " kilogramos."
					Escribir "equivalente a: ", lib " libras"
					Escribir "y equivalente a: ", onz " onzas"
				2:
					g = p*1000
					lib1 = p*2.20462
					onz1 = p*35.274
					Escribir p " kilogramos equivalen a: ", g " gramos."
					Escribir "equivalente a: ", lib1 " libras"
					Escribir "y equivalente a: ", onz1 " onzas"
				3:
					g3 = p*453.592
					kg3 = p/2.20462
					onz3 = p*16
					Escribir p " libras equivalen a: ", g3 " gramos."
					Escribir "equivalente a: ", kg3 " kilogramos."
					Escribir "y equivalente a: ", onz3 " onzas."
				4:
					g4 = p*28.3495
					kg4 = p/35.274
					lib4 = p/16
					Escribir p " onzas equivalen a: ", g4 " gramos."
					Escribir "equivalente a: ", kg4 " kilogramos"
					Escribir "y equivalente a: ", lib4 " libras"
				De Otro Modo:
					Escribir "opcion no valida"
			Fin Segun
		3:
			Escribir "ingrese la velocidad"
			Leer v
			Escribir "�En que unidad esta ese valor?"
			Escribir ""
			Escribir "1. metro por segundo (m/s)"
			Escribir "2. Kilometro por hora (Km/h)"
			Leer univ
			Segun univ Hacer
				1:
					kmh = v*3.6
					Escribir v "m/s equivalen a: ", kmh "Km/h"
				2:
					mh = v/3.6
					Escribir v "Km/h equivalen a: ", mh "m/s"
				De Otro Modo:
					Escribir "opcion no valida"
			Fin Segun
		De Otro Modo:
			Escribir "opcion no valida"
	Fin Segun
	
FinAlgoritmo
