Algoritmo Temperatura_de_marte_en_julio
	Dimension Julio(5,7)
	Dimension Julio2(5,7)
	Tbaja=30
	Talta=-150
	Para i=0 Hasta 4 Hacer
		Para j=0 Hasta 6 Hacer
			Julio(i,j)=Azar(160)-140
			aux=aux+1
			Julio2(i,j)=aux
			Si i=4 y j>=3 Entonces
				Julio(i,j)=0
				Julio2(i,j)=0
			FinSi
			Si Julio(i,j)<Tbaja Entonces
				Tbaja=Julio(i,j)
				DNbaja=Julio2(i,j)
				jbaja=j
			FinSi
			Si Julio(i,j)>Talta Entonces
				Talta=Julio(i,j)
				DNalta=Julio2(i,j)
				jalta=j
			FinSi
		FinPara
	FinPara
	Segun jbaja Hacer
		0:
			Dbaja="lunes"
		1:
			Dbaja="martes"
		2:
			Dbaja="miercoles"
		3:
			Dbaja="jueves"
		4:
			Dbaja="viernes"
		5:
			Dbaja="sabado"
		6:
			Dbaja="domingo"
	FinSegun
	Segun jalta Hacer
		0:
			Dalta="lunes"
		1:
			Dalta="martes"
		2:
			Dalta="miercoles"
		3:
			Dalta="jueves"
		4:
			Dalta="viernes"
		5:
			Dalta="sabado"
		6:
			Dalta="domingo"
	FinSegun
	proms1=Trunc((Julio(0,0)+Julio(0,1)+Julio(0,2)+Julio(0,3)+Julio(0,4)+Julio(0,5)+Julio(0,6))/7)
	proms2=Trunc((Julio(1,0)+Julio(1,1)+Julio(1,2)+Julio(1,3)+Julio(1,4)+Julio(1,5)+Julio(1,6))/7)
	proms3=Trunc((Julio(2,0)+Julio(2,1)+Julio(2,2)+Julio(2,3)+Julio(2,4)+Julio(2,5)+Julio(2,6))/7)
	proms4=Trunc((Julio(3,0)+Julio(3,1)+Julio(3,2)+Julio(3,3)+Julio(3,4)+Julio(3,5)+Julio(3,6))/7)
	proms5=Trunc((Julio(4,0)+Julio(4,1)+Julio(4,2))/3)
	Escribir "     Temperatura planeta Marte - Julio/A�o 2019"
	Escribir ""
	Escribir "La temperatura m�s baja fue el ",Dbaja," ",DNbaja," con ",Tbaja,"�C"
	Escribir ""
	Escribir "La temperatura m�s alta fue el ",Dalta," ",DNalta," con ",Talta,"�C"
	Escribir ""
	Escribir "El promedio de temperatura de la semana 1 fue ",proms1
	Escribir "El promedio de temperatura de la semana 2 fue ",proms2
	Escribir "El promedio de temperatura de la semana 3 fue ",proms3
	Escribir "El promedio de temperatura de la semana 4 fue ",proms4
	Escribir "yy el de los d�as que alcanz� la semana 5 fue ",proms5
FinAlgoritmo
