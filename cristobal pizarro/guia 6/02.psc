Algoritmo Menu_Arreglos
	Escribir "Ingrese el tama�o de los arreglos"
	Leer tama�o
	Mientras terminar!=1 Hacer
		Escribir "Ingrese el n�mero de la acci�n que desea realizar"
		Escribir "[1] Llenar un arreglo A de manera aleatoria"
		Escribir "[2] Llenar un arreglo B de manera aleatoria"
		Escribir "[3] Crear un arreglo C = A + B"
		Escribir "[4] Crear un arreglo C = A - B"
		Escribir "[5] Mostrar un arreglo en especifico"
		Escribir "O cualquier otro n�mero para salir"
		Leer option
		Segun option Hacer
			1:
				Dimension A(tama�o)
				Para i=0 Hasta tama�o-1 Hacer
					A(i)=Azar(50)
				FinPara
				auxA=1
				Escribir ""
				Escribir "Arreglo A creado"
				Escribir ""
			2:
				Dimension B(tama�o)
				Para i=0 Hasta tama�o-1 Hacer
					B(i)=Azar(50)
				FinPara
				auxB=1
				Escribir ""
				Escribir "Arreglo B creado"
				Escribir ""
			3:
				Si auxA+auxB=2 Entonces
					Dimension C(tama�o)
					Para i=0 Hasta tama�o-1 Hacer
						C(i)=A(i)+B(i)
					FinPara
					Escribir ""
					Escribir "Arreglo C creado"
					Escribir ""
				SiNo
					Escribir ""
					Escribir "Aun no estan creados los arreglos A y B"
					Escribir ""
				FinSi
			4:
				Si auxA+auxB=2 Entonces
					Dimension C(tama�o)
					Para i=0 Hasta tama�o-1 Hacer
						C(i)=A(i)-B(i)
					FinPara
					Escribir ""
					Escribir "Arreglo C creado"
					Escribir ""
				SiNo
					Escribir ""
					Escribir "Aun no estan creados los arreglos A y B"
					Escribir ""
				FinSi
			5:
				Escribir "Ingrese la letra del arreglo que desea ver"
				Leer letra
				letra=Mayusculas(letra)
				Segun letra Hacer
					"A":
						Escribir ""
						Para i=0 Hasta tama�o-1 Hacer
							Escribir Sin Saltar " ",A(i)
						FinPara
						Escribir ""
						Escribir ""
					"B":
						Escribir ""
						Para i=0 Hasta tama�o-1 Hacer
							Escribir Sin Saltar " ",B(i)
						FinPara
						Escribir ""
						Escribir ""
					"C":
						Escribir ""
						Para i=0 Hasta tama�o-1 Hacer
							Escribir Sin Saltar " ",C(i)
						FinPara
						Escribir ""
						Escribir ""
					De Otro Modo:
						Escribir ""
						Escribir "Ese arreglo no existe"
						Escribir ""
				FinSegun
			De Otro Modo:
				terminar=1
		FinSegun
	FinMientras
FinAlgoritmo
