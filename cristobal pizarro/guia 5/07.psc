Algoritmo Ceros_10x10_diagonal_1
	Dimension metapod[10,10]
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			metapod[i,j]=0
		FinPara
	FinPara
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			Si i==j Entonces
				metapod[i,j]=1
			FinSi
		FinPara
	FinPara
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			Escribir Sin Saltar " ",metapod[i,j]
		FinPara
		Escribir ""
	FinPara
FinAlgoritmo
