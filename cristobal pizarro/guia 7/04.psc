Algoritmo Influencers
	Dimension mat[4,5]
	Dimension rs[4]
	Dimension est[5]
	rs[0]="Facebook"
	rs[1]="Twitter"
	rs[2]="Instagram"
	rs[3]="Snapchat"
	est[0]="Maxi"
	est[1]="Kensel"
	est[2]="Fabian"
	est[3]="Javi"
	est[4]="Vania"
	mlikes=0
	rmlikes=0
	mglikes=0
	Para i=0 Hasta 3 Hacer //Aqui se llena la matriz y se guardan los valores especiales
		Para j=0 Hasta 4 Hacer
			mat[i,j]=Azar(250)
			rlikes=rlikes+mat[i,j] //Esto suma los likes por red social
			Si mat[i,j]>mlikes Entonces //Esto guarda quien tuvo mas likes y en que red social
				mlikes=mat[i,j]
				mlikesr=i
				mlikese=j
			FinSi
		FinPara
		Si rlikes>rmlikes Entonces //Esto guarda la red social que gener� mas likes
			rmlikes=rlikes
			rclikes=i
		FinSi
		rlikes=0
	FinPara
	Dimension mlikesg[4] //Esto es para guardar quien tuvo mas likes en general
	mlikesg[0]=mat[0,0]+mat[1,0]+mat[2,0]+mat[3,0]
	mlikesg[1]=mat[0,1]+mat[1,1]+mat[2,1]+mat[3,1]
	mlikesg[2]=mat[0,2]+mat[1,2]+mat[2,2]+mat[3,2]
	mlikesg[3]=mat[0,3]+mat[1,3]+mat[2,3]+mat[3,3]
	Para i=0 Hasta 3 Hacer
		Si mlikesg[i]>mglikes Entonces
			mglikes=mlikesg[i]
			mglikese=i
		FinSi
	FinPara
	Mientras terminar!=1 Hacer //Y este es el menu
		Escribir "Ingrese el n�mero de la accion que desea realizar"
		Escribir "[1] Mostrar quien tuvo mas likes y en que red social"
		Escribir "[2] Ver Cual red social es la que genera m�s likes"
		Escribir "[3] Mostrar quien tuvo mas likes en general"
		Escribir "[4] Mostrar una tabla con toda la informaci�n"
		Escribir "[5] Consultar por alguien en especifico"
		Escribir "O Ingrese cualquier otro n�mero para salir"
		Leer option
		Segun option Hacer
			1: 
				Escribir ""
				Escribir est[mlikese]," tuvo mas likes en ",rs[mlikesr]
				Escribir ""
			2:
				Escribir ""
				Escribir rs[rclikes]," genera mas likes con un total de ",rmlikes
				Escribir ""
			3:
				Escribir ""
				Escribir est[mglikese]," tuvo mas likes en general con un total de ",mglikes
				Escribir ""
			4: 
				Escribir ""
				Escribir Sin Saltar "          "
				Para i=0 Hasta 4 Hacer
					Si i=0 o i=3 Entonces
						Escribir Sin Saltar est[i],"   "
					SiNo
						Si i=4 Entonces
							Escribir Sin Saltar est[i],"  "
						SiNo
							Escribir Sin Saltar est[i]," "
						FinSi
					FinSi
				FinPara
				Escribir ""
				Para i=0 Hasta 3 Hacer
					Para j=0 Hasta 4 Hacer
						Si i=0 Entonces
							Mientras f!=1 Hacer
								Escribir Sin Saltar rs[0]," "
								f=1
							FinMientras
						FinSi
						Si i=1 Entonces
							Mientras t!=1 Hacer
								Escribir Sin Saltar rs[1],"  "
								t=1
							FinMientras
						FinSi
						Si i=2 Entonces
							Mientras in!=1 Hacer
								Escribir Sin Saltar rs[2]
								in=1
							FinMientras
						FinSi
						Si i=3 Entonces
							Mientras s!=1 Hacer
								Escribir Sin Saltar rs[3]," "
								s=1
							FinMientras
						FinSi
						Si mat[i,j]<10 Entonces
							Escribir Sin Saltar" ",mat[i,j],"     "
						SiNo
							Si mat[i,j]<100 Entonces
								Escribir Sin Saltar" ",mat[i,j],"    "
							SiNo
								Escribir Sin Saltar" ",mat[i,j],"   "
							FinSi
						FinSi
					FinPara
					Escribir ""
				FinPara
				Escribir ""
				f=0
				t=0
				in=0
				s=0
			5:
				Escribir "Ingrese el n�mero del influencer que desea consultar"
				Escribir "[1] Maxi"
				Escribir "[2] Kensel"
				Escribir "[3] Fabian"
				Escribir "[4] Javi"
				Escribir "[5] Vania"
				Leer option2
				Segun option2 Hacer
					1:
						Escribir ""
						Escribir "              Maxi"
						Escribir "Facebook Twitter Instagram Snapchat"
						Escribir "  ",mat[0,0],"       ",mat[1,0],"      ",mat[2,0],"      ",mat[3,0]
						Escribir ""
					2:
						Escribir ""
						Escribir "              Kensel"
						Escribir "Facebook Twitter Instagram Snapchat"
						Escribir "  ",mat[0,1],"       ",mat[1,1],"      ",mat[2,1],"      ",mat[3,1]
						Escribir ""
					3:
						Escribir ""
						Escribir "              Fabian"
						Escribir "Facebook Twitter Instagram Snapchat"
						Escribir "  ",mat[0,2],"       ",mat[1,2],"      ",mat[2,2],"      ",mat[3,2]
						Escribir ""
					4:
						Escribir ""
						Escribir "              Javi"
						Escribir "Facebook Twitter Instagram Snapchat"
						Escribir "  ",mat[0,3],"       ",mat[1,3],"      ",mat[2,3],"      ",mat[3,3]
						Escribir ""
					5:
						Escribir ""
						Escribir "              Vania"
						Escribir "Facebook Twitter Instagram Snapchat"
						Escribir "  ",mat[0,4],"       ",mat[1,4],"      ",mat[2,4],"      ",mat[3,4]
						Escribir ""
					De Otro Modo:
						Escribir "Error"
				FinSegun
			De Otro Modo: //Ingresando cualquier otro n�mero termina el ciclo
				terminar=1
		FinSegun
	FinMientras
FinAlgoritmo
