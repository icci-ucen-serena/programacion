Algoritmo Promedios_carrera_ICCI
	Dimension ICCI[5,5]
	Dimension PICCI[5]
	pp=80
	Para i=0 Hasta 4 Hacer
		s=0
		Para j=0 Hasta 4 Hacer
			ICCI[i,j]=Azar(70)
			s=s+ICCI[i,j]
			Si j=0 Entonces
				Si ICCI[i,j]<40 Entonces
					iai=iai+1
					piai=piai+ICCI[i,j]
				FinSi
			FinSi
			Si j=1 Entonces
				Si ICCI[i,j]<40 Entonces
					iam=iam+1
					piam=piam+ICCI[i,j]
				FinSi
			FinSi
			Si j=2 Entonces
				Si ICCI[i,j]<40 Entonces
					iaf=iaf+1
					piaf=piaf+ICCI[i,j]
				FinSi
			FinSi
			Si j=3 Entonces
				Si ICCI[i,j]<40 Entonces
					adm=adm+1
					padm=padm+ICCI[i,j]
				FinSi
			FinSi
			Si j=4 Entonces
				PICCI[i]=s/5
				Si PICCI[i]<pp Entonces
					pp=PICCI[i]
					ppi=i
				FinSi
				Si PICCI[i]>mp Entonces
					mp=PICCI[i]
					mpi=i
				FinSi
				Si ICCI[i,j]<40 Entonces
					prg=prg+1
					pprg=pprg+ICCI[i,j]
				FinSi
			FinSi
		FinPara
	FinPara
	Dimension pasignaturas[5]
	pasignaturas[0]=piai/5
	pasignaturas[1]=piam/5
	pasignaturas[2]=piaf/5
	pasignaturas[3]=padm/5
	pasignaturas[4]=pprg/5
	ppa=80
	Para i=0 Hasta 4 Hacer
		Si pasignaturas[i]<ppa Entonces
			ppa=pasignaturas[i]
			appa=i
		FinSi
	FinPara
	Dimension rasignaturas[5]
	rasignaturas[0]=iai
	rasignaturas[1]=iam
	rasignaturas[2]=iaf
	rasignaturas[3]=adm
	rasignaturas[4]=prg
	Para i=0 Hasta 4 Hacer
		Si rasignaturas[i]>amr Entonces
			amr=rasignaturas[i]
			aamr=i
		FinSi
	FinPara
	Dimension asignaturas[5]
	asignaturas[0]="Introducción a ingeniería"
	asignaturas[1]="Introducción a matemáticas"
	asignaturas[2]="Introducción a física"
	asignaturas[3]="Administración"
	asignaturas[4]="Programación"
	Mientras terminar!=1 Hacer
		Escribir "Ingrese el número de lo que desea mostrar"
		Escribir "[1] Estudiantes con promedio rojo"
		Escribir "[2] El estudiante con peor promedio"
		Escribir "[3] El estudiante con mejor promedio"
		Escribir "[4] La asignatura con mas rojos"
		Escribir "[5] La asignatura del peor promedio"
		Escribir "O ingrese cualquier otro número para salir"
		Leer option
		Segun option Hacer
			1:
				Escribir ""
				Escribir Sin Saltar "Estudiantes "
				Para i=0 Hasta 4 Hacer
					Si PICCI[i]<40 Entonces
						Escribir Sin Saltar i+1," "
					FinSi
				FinPara
				Escribir ""
				Escribir ""
			2:
				Escribir ""
				Escribir "Estudiante ",ppi+1, " con un ",pp/10
				Escribir ""
			3:
				Escribir ""
				Escribir "Estudiante ",mpi+1, " con un ",mp/10
				Escribir ""
			4:
				Escribir ""
				Escribir "La asignatura con mas rojos es ",asignaturas[aamr]
				Escribir ""
			5:
				Escribir ""
				Escribir "La asignatura con peor promedio general es ",asignaturas[appa]
				Escribir ""
			De Otro Modo:
				terminar=1
		FinSegun
	FinMientras
FinAlgoritmo
