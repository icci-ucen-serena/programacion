Algoritmo guia_diez
	Dimension hola[10,10]
	
    Para i = 0 Hasta 9 Hacer 
        Para j=0 Hasta 9 Hacer 
            Si i%2==0 Entonces
				Si j%2!=0 entonces
					hola[i,j]=1
				FinSi
            SiNo 
				Si j%2=0 Entonces
					hola[i,j]=1
				SiNo
					hola[i,j]=0
				FinSi
			FinSi
		FinPara
    FinPara
	
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			Escribir Sin Saltar " ",hola[i,j]
		FinPara
		Escribir " "
	FinPara
	
FinAlgoritmo
