Algoritmo guia_ocho
	
	Dimension hola[10,10]
	
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			Si i<j Entonces
				hola[i,j]=1
		    SiNo
				hola[i,j]=0
			FinSi
		FinPara
	FinPara
	
	Para i=0 Hasta 9 Hacer
		Para j=0 Hasta 9 Hacer
			Escribir Sin Saltar " ",hola[i,j]
		FinPara
		Escribir " "
	FinPara
	
FinAlgoritmo
