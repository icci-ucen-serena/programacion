Subproceso retorno <-suma (x1,y1)
	Escribir "Estamos sumando para usted"
	retorno <-x1+y1
FinSubProceso

SubProceso retorno <-resta (x1,y1)
	Escribir "Estamos restando para usted"
	retorno <-x1-y1
FinSubProceso

SubProceso retorno<-producto (x1,y1)
	Escribir "Estamos multiplicando para usted"
	retorno <-x1*y1
FinSubProceso

SubProceso retorno<-cociente (x1,y1)
	Escribir "Estamos dividiendo para usted"
	retorno <-x1/y1
FinSubProceso

Algoritmo Ejercicio
	Escribir "Ingrese el n�mero de la tarea que quiera realizar"
	Escribir "(0) Salir de la calculadora"
	Escribir "(1) Sumar dos n�meros"
	Escribir "(2) Restar dos n�meros"
	Escribir "(3) Multiplicar dos n�meros"
	Escribir "(4) Dividir dos n�meros"
	Leer operacion
	
	Si operacion=0 Entonces
		Escribir "Ha salido de la calculadora"
	FinSi
	
	Mientras operacion!=0 Hacer
		
		Segun operacion Hacer
	
			1:
				Escribir "Ingrese dos n�meros para ser sumados"
				Leer n1,n2
				sum = suma(n1,n2)
				Escribir "El resultado es ",sum
			2:
				Escribir "Ingrese dos n�meros para ser restados"
				Leer n1,n2
				rest = resta(n1,n2)
				Escribir "El resultado es ",rest
				
			3:
				Escribir "Ingrese dos n�meros para ser multiplicados"
				Leer n1,n2
				product = producto(n1,n2)
				Escribir "El resultado es ",product
				
			4: 
				Escribir "Ingrese dos n�meros para ser divididos"
				Leer n1,n2
				cocient = cociente(n1,n2)
				Escribir "El resultado es ",cocient	
				
			De Otro Modo:
				Escribir "La opci�n digitada no es v�lida"
				
		Fin Segun
		
		Escribir "Ingrese el n�mero de la tarea que quiera realizar"
		Escribir "(0) Salir de la calculadora"
		Escribir "(1) Sumar dos n�meros"
		Escribir "(2) Restar dos n�meros"
		Escribir "(3) Multiplicar dos n�meros"
		Escribir "(4) Dividir dos n�meros"
		Leer operacion
		
	Fin Mientras
	
	
	
FinAlgoritmo
