Algoritmo guia6ejercicio4
	Dimension julio[5,7]
	Dimension temp[5,7]
	cont=1
	tbajo=30
	talto=-150
	dbajo=0
	dalto=0
	Escribir "TEMPERATURAS JULIO MARTE"
	Para i=0 hasta 4 hacer
		Para j=0 hasta 6 Hacer
			julio[i,j]=cont
			temp[i,j]=azar(160)-140
			cont=cont+1
			Si temp[i,j]<tbajo Entonces
				tbajo=temp[i,j]
				dbajo=julio[i,j]
			SiNo
				Si temp[i,j]>talto Entonces
					talto=temp[i,j]
					dalto=julio[i,j]
				FinSi
			FinSi
			Si julio[i,j]>31 Entonces
				julio[i,j]=0
				temp[i,j]=0
			FinSi
			Escribir Sin Saltar "D�a " julio[i,j] " "
			Escribir Sin Saltar temp[i,j] "C " " "
		FinPara
		Escribir ""
	FinPara

	Escribir ""
	Escribir "El d�a ", dalto " es el d�a con mayor temperatura, alcanzando los ", talto "�C"
	Escribir ""
	Escribir "El d�a ", dbajo " es el d�a con menor temperatura, alcanzando los ", tbajo "�C"
FinAlgoritmo
