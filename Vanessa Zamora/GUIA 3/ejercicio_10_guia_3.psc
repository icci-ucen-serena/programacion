//Escriba un programa que pida al usuario dos n�meros enteros, y luego entregue la suma de todos los n�meros que est�n entre ellos. Por ejemplo, si los n�meros son 1 y 7, debe entregar como resultado 2 + 3 + 4 + 5 + 6 = 20.
Algoritmo sumanumerosenteros
	Escribir "Ingrese dos n�meros enteros,para sumar todos los n�meros que se encuentran entre ellos"
	leer v, b
	resultado=0
	Para u<-v+1 Hasta b-1 Con Paso 1 Hacer
		resultado=resultado+u
	Fin Para
	Para u<-v+1 Hasta b-1 Con Paso 1 Hacer
		Si u==b-1
			Escribir Sin Saltar ,u " = "
	    SiNo 
			Escribir Sin Saltar ,u " + "
		FinSi
	Fin Para
	Escribir resultado
FinAlgoritmo
