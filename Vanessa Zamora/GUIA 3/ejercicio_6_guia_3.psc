//El �ndice de masa corporal (IMC) se determina dividiendo el peso (kg) por el cuadrado de la estatura (mt) ? IMC = peso/altura�. Elabore un algoritmo que calcule el IMC ingresando el peso y la altura de una persona. Si el �ndice es mayor que 22.0 muestre un mensaje de alerta por riesgo a enfermedades coronarias.
Algoritmo IMC
	Escribir "Ingrese su peso y altura(en m) para calcular su IMC"
	leer peso,altura
	resultado =peso/(altura^2)
	Escribir "Su IMC es de ",resultado
	Si resultado>22 Entonces
		Escribir "�ALERTA! USTED TIENE MUCHO RIESGO DE CONTRAER ENFERMEDADES CORONARIAS"
	FinSi
FinAlgoritmo
